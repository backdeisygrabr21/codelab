package codelab.bbva;


import codelab.bbva.data.CarroMongo;
import codelab.bbva.data.CarroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//@SpringBootApplication
public class CarroV2Controller implements CommandLineRunner {

    @Autowired
    private CarroRepository repository;

    public static void main(String[] args){
        SpringApplication.run(CarroV2Controller.class, args);
    }



    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando MongoDB");
        repository.insert(new CarroMongo("Mazda",2019,200.92,5,"Automático"));
        repository.insert(new CarroMongo("Chevrolet",2012,198.23,5,"Mecanico"));
        repository.insert(new CarroMongo("Audit",2020,1258.05,5,"Automático"));
        repository.insert(new CarroMongo("BMW",2021,129.23,5,"Mecanico"));
        repository.insert(new CarroMongo("Volkswagen",2022,200.92,5,"Automático"));
        repository.insert(new CarroMongo("Mazda",2012,158.92,5,"Mecanico"));
        List<CarroMongo> lista = repository.findAll();
        for (CarroMongo p:lista){
            System.out.println(p.toString());
        }
    }
}
