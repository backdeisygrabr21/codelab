package codelab.bbva;

import codelab.bbva.data.CarroMongo;
import codelab.bbva.data.CarroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class CarroV3Controller {
    @Autowired
    private CarroRepository repository;

    /*Consulta la lista de carros */
    @GetMapping(value = "mongodb/carros", produces = "application/json")
    public ResponseEntity<List<CarroMongo>> obtenerListado()
    {
        List<CarroMongo> lista = repository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    /*Obtiene carro por ID */
    @GetMapping(value = "/mongodb/carrosbyid/{id}", produces = "application/json")
    public ResponseEntity<CarroMongo> obtenerCarroPorId(@PathVariable String id)
    {
        Optional<CarroMongo> resultado = repository.findById(id);
        CarroMongo carroAConsultar = null;
        if (resultado.isPresent())
        {
            carroAConsultar = resultado.get();
            return new ResponseEntity<CarroMongo>(carroAConsultar,HttpStatus.OK);
        }
        return new ResponseEntity<CarroMongo>(carroAConsultar,HttpStatus.NOT_FOUND);
    }

    /* Crea un carro */
    @PostMapping(value="/mongodb/carros")
    public ResponseEntity<String>addCarro(@RequestBody CarroMongo carroMongo){
        CarroMongo resutado = repository.insert(carroMongo);
        return new ResponseEntity<String>(resutado.toString(),HttpStatus.OK);
    }

    /*Actualiza un Carro */
    @PutMapping(value = "/mongodb/carros/{id}")
    public ResponseEntity<String> updateCarro(@PathVariable String id, @RequestBody CarroMongo carroMongo)
    {
        Optional <CarroMongo> resultado = repository.findById(id);
        if (resultado.isPresent()){
            CarroMongo carrroAModificar = resultado.get();
            carrroAModificar.marca = carroMongo.marca;
            carrroAModificar.modelo = carroMongo.modelo;
            carrroAModificar.precio = carroMongo.precio;
            carrroAModificar.ocupantes = carroMongo.ocupantes;
            carrroAModificar.tipo = carroMongo.tipo;
            CarroMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Carro no encontrado",HttpStatus.NOT_FOUND);
    }

    /*Borra un carro */
    @DeleteMapping(value="/mongodb/carros/{id}")
    public ResponseEntity<String>deleteCarro(@PathVariable String id)
    {
        repository.deleteById(id);
        return new ResponseEntity<String>("Carro borrado",HttpStatus.OK);
    }


}
