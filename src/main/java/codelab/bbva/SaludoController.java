package codelab.bbva;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {
    @RequestMapping("/")
    public String index(){
        return "Hola soy tu API codelab";
    }
}
