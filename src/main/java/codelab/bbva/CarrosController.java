package codelab.bbva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class CarrosController {
    private ArrayList<CarroModelo> listaCarros = null;

    public CarrosController() {
        listaCarros = new ArrayList<>();
        listaCarros.add(new CarroModelo(1, "Volkswagen Gol", 2022, 650.00, 5, "Mecanico"));
        listaCarros.add(new CarroModelo(2, "Mazda", 2020, 350.05, 5, "Automático"));
        listaCarros.add(new CarroModelo(3, "Chevrolet", 2012, 500.02, 5, "Mecanico"));

    }

    /*Get Lista de carros */
    @GetMapping(value = "/carros", produces = "application/json")
    public ResponseEntity<List<CarroModelo>> getListado() {
        return new ResponseEntity<>(listaCarros, HttpStatus.OK);
    }

    /*Get carro por ID */
    @GetMapping("/carros/{id}")
    public ResponseEntity<CarroModelo> obtenerProductoPorId(@PathVariable int id)
    {
        CarroModelo resultado = null;
        ResponseEntity<CarroModelo> respuesta = null;
        try
        {
            resultado = listaCarros.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add nuevo carro */
    @PostMapping(value = "/carros", produces="application/json")
            public ResponseEntity<String>addCarro(@RequestBody CarroModelo carroNuevo)
    {
        System.out.println(carroNuevo.getId());
        System.out.println(carroNuevo.getMarca());
        System.out.println(carroNuevo.getModelo());
        System.out.println(carroNuevo.getPrecio());
        System.out.println(carroNuevo.getOcupantes());
        System.out.println(carroNuevo.getTipo());
        listaCarros.add(carroNuevo);
        return new ResponseEntity<>("Carro creado correctamente",HttpStatus.CREATED);
    }

    /*Modificar modelo y precio de los carros */
    @PutMapping("/carros/{id}")
    public ResponseEntity<String> updateCarro(@PathVariable int id, @RequestBody CarroModelo cambios)
    {
        ResponseEntity<String> resultado = null;
        try {
            CarroModelo carroAModificar = listaCarros.get(id);
            System.out.println("Voy a modificar el carro");
            System.out.println("Modelo Actual:" + String.valueOf(carroAModificar.getModelo()));
            System.out.println("Precio actual: " + String.valueOf(carroAModificar.getPrecio()));
            carroAModificar.setModelo(cambios.getModelo());
            carroAModificar.setPrecio(cambios.getPrecio());
            listaCarros.set(id,carroAModificar);
            resultado = new ResponseEntity<>("Carro Actualizado correctamente",HttpStatus.OK);
            }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("Carro no encontrado", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*Borra carro por ID */
    @DeleteMapping("/carros/{id}")
    public ResponseEntity<String> deleteCarro(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            CarroModelo carroAEliminar = listaCarros.get(id-1);
            listaCarros.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("Carro no encontrado",HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}
