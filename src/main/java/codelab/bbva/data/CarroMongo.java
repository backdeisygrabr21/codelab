package codelab.bbva.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document("CarrosDeisy")
public class CarroMongo {

    @Id
    public String id;
    public String marca;
    public int modelo;
    public double precio;
    public int ocupantes;
    public String tipo;

    public  CarroMongo(){

    }

    public CarroMongo(String marca, int modelo, double precio, int ocupantes, String tipo) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.ocupantes = ocupantes;
        this.tipo = tipo;
    }
    @Override
    public String toString(){
        return String.format("Carro [id=%s, marca=%s, modelo=%s, precio=%s, ocupantes=%s, tipo=%s]",id, marca, modelo, precio, ocupantes, tipo);
    }
}
